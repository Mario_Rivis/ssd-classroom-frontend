import {Component, OnInit} from '@angular/core';
import {Student} from './student/student';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'ssdcr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ssd-classroom-frontend';
  students: Student[] = [
    {
      name: 'John',
      age: 50,
      isPresent: true
    },
    {
      name: 'Ana',
      age: 40,
      isPresent: false
    },
    {
      name: 'DAn',
      age: 20,
      isPresent: true
    },
    {
      name: 'Ioana',
      age: 80,
      isPresent: true,
    },
    {}
  ];

  constructor(private httpClient: HttpClient) {
  }

  ngOnInit(): void {
    this.httpClient.get('http://localhost:7070/api/students').subscribe(
      (value: any[]) => {
        console.log(value);
        this.students = this.students.concat(value);
      }
    );
  }
}
