import {Component, Input, OnInit} from '@angular/core';
import {MatSlideToggleChange} from '@angular/material';

@Component({
  selector: 'ssdcr-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  @Input()
  name = 'John';

  @Input()
  age = 20;

  @Input()
  isPresent: boolean;

  constructor() {
  }

  ngOnInit() {
  }

  onNameClicked() {
    console.log('Name is clicked');
  }

  onPresentChange($event: MatSlideToggleChange) {
    this.isPresent = $event.checked;
  }
}
